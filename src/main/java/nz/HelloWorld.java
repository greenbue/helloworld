package nz;

import javax.faces.bean.ManagedBean;

@ManagedBean(name = "helloWorld", eager = true)
public class HelloWorld {

  public HelloWorld() {
    System.out.println("HelloWorld started!");
  }

  public String getMessage() {
    return "Hello World!";
  }

  public String getMoreInformation(boolean var) {
    if (var == true) {
      return "If you would like more information, contact me on test@test.com";
    }
    else {
      return "Thank you for your email, we will get back to you as soon as possible.";
    }

  }
}